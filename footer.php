<!-- Footer Widget Secton -->
<footer>
	<div class="container-fluid scoreline-footer space">
		<div class="container">
			<?php 
			if ( is_active_sidebar( 'footer-widget-area' ) ){ 
				dynamic_sidebar( 'footer-widget-area' );
			} ?>		
	    </div>			
	</div>			

	<div class="container-fluid footerweb">
		<div class="footer_area">
			<div class="container">
				<div class="col-md-8 scoreline-copyryt">
					<?php if ( scoreline_theme_is_companion_active() ) { ?>	
						<p class="scoreline_footer_text">
							<?php 
							$scoreline_footer_customization = get_theme_mod( 'scoreline_footer_customization', __('&copy; Copyright 2020. All Rights Reserved','scoreline') );
							echo esc_html( $scoreline_footer_customization ); ?>
							<a target="_blank" rel="nofollow" href="<?php echo esc_url( get_theme_mod( 'scoreline_deve_link' ) ); ?>">
								<?php 
								$scoreline_develop_by = get_theme_mod( 'scoreline_develop_by' );
								echo esc_html( $scoreline_develop_by ); ?>
							</a>
						</p>
					<?php 
			        }else{ ?>
				        <p class="scoreline_footer_text">
			                <?php esc_html_e('&copy; Copyright 2020. All Rights Reserved','scoreline'); ?>
			            </p>  
				    <?php } ?>
				</div>

				<?php $footer_section_social_media_enbled = absint(get_theme_mod('footer_section_social_media_enbled', 1));
				if ($footer_section_social_media_enbled == 1) { ?>
					<div class="col-md-4 scoreline-copyryt scoreline_social_media_footer">
						<ul class="social-icons">
							<?php 
							$fb_link = get_theme_mod('fb_link');
	                    	if (!empty ($fb_link)) { ?>
								<li class="facebook"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
							<?php }
							$twitter_link = get_theme_mod('twitter_link');
	                    	if (!empty ($twitter_link)) { ?>
								<li class="twitter"><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
							<?php } 
							$youtube_link = get_theme_mod('youtube_link');
	                    	if (!empty ($youtube_link)) { ?>
								<li class="youtube"><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
							<?php } 
							$linkedin_link = get_theme_mod('linkedin_link');
	                        if (!empty ($linkedin_link)) { ?>
								<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin-in"></i></a></li>
							<?php } 
							$instagram = get_theme_mod('instagram');
	                    	if (!empty ($instagram)) { ?>
	                    		<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
	                    	<?php } ?>
						</ul>
					</div>	
				<?php } ?>
			</div>		
		</div>
	</div>
</footer>	
<?php if ( scoreline_theme_is_companion_active() ) { 
    $scoreline_return_top = absint(get_theme_mod( 'scoreline_return_top', '1' ));
    if ( $scoreline_return_top == "1" ) { ?>
		<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
	<?php } 
} ?>
<?php wp_footer(); ?>
</div>
</body>
</html>
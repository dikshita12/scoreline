<!-- extra section -->
<div class="container-fluid extra space">
	<div class="container">
		
		<h1 class="scoreline_extra_title"><?php echo esc_html(get_theme_mod('extra_sec_titl')); ?></h1>		
	
		<p class="team-text scoreline_extra_desc">
			<?php echo wp_kses_post(get_theme_mod('extra_sec_desc')); ?>
		</p>
		
	</div>
</div>
<!-- /extra section -->
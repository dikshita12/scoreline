<div class="scoreline-callout-wrapper">
	<div class="container-fluid scoreline-callout space">
	    <div class="container">
			<div class="row scoreline-callout-desc scoreline_call_out_title">
		        <?php 
				$fc_title = get_theme_mod( 'fc_title', __('Lorem Ipsum is simply dummy text of the printing and typesetting industry. ','scoreline') );
				if ( ! empty ( $fc_title ) ) { ?>
					<div class="col-md-12">
					    <h2><?php echo esc_html( $fc_title); ?></h2>
					</div>
			    <?php } ?>
				<div class="">
				<?php $fc_btn_txt = get_theme_mod( 'fc_btn_txt', __('More Features','scoreline') );
				if ( ! empty ( $fc_btn_txt ) ) { ?>
					<div class="col-md-12 btn scoreline_call_out_buttun">
						<a href="<?php echo esc_url( get_theme_mod( 'fc_btn_link') ); ?>"><?php echo esc_html( $fc_btn_txt  ); ?></a>
					</div>
				<?php } ?>
		        </div>
	        </div>
        </div>
    </div>
</div>
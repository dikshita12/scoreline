<!-- Blog-start -->
<div class="container-fluid scoreline-blog space">
	<div class="container">
		<?php 
        $blog_title1 = get_theme_mod('blog_title', __('Latest Blog','scoreline'));
        if ( ! empty ( $blog_title1 ) ) { ?>
			<h1 class="scoreline_blog_title"><?php echo esc_html( $blog_title1 ); ?></h1>
		<?php } ?>	
				 
        <div class="row scoreline-blog-desc">
	        <?php 		
				$posts_count =wp_count_posts()->publish;
				$args = array( 'post_type' => 'post','posts_per_page' => $posts_count ,'ignore_sticky_posts' => 1);		
				$post_type_data = new WP_Query( $args );
				if($post_type_data->have_posts()){ ?>
					
				<div class="slider-wrapper">
					<div class="swiper-container home_blog">
						<div class="swiper-wrapper">
					        <?php while($post_type_data->have_posts()):
							$post_type_data->the_post();  ?>	
							    <div class="swiper-slide">
							        <div class="col-md-12 scoreline-blog-text">					
										<div class="latest-post">
											<?php if(has_post_thumbnail()): ?>
												<?php $data= array('class' =>'img-responsive'); 
												the_post_thumbnail('scoreline-post-thumb', $data); ?>
											   <a href="<?php echo esc_url(wp_get_attachment_url(get_post_thumbnail_id())); ?>"></a>
											<?php endif; ?>
											<div class="post-body">
												<h3 class="post-title">
													<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
												</h3>
												<div class="post-tag">
													<?php if(get_the_tag_list() !='') { ?>
														<?php the_tags( __('<span>Tags :</span> ','scoreline'), ' ', '<br />'); ?>
													<?php } ?>	
												</div>
												<div class="post-text">
													<?php the_excerpt(); ?>
												</div>
											</div>	
											<div class="post-bottom">
												<div class="pull-left">
												<i class="fa fa-calendar"></i>
													<?php the_date(); ?>
												</div>
												<?php $read_more = get_theme_mod( 'read_more', __('Continue','scoreline') ) ?>
												<a class="pull-right" href="<?php the_permalink(); ?>"><?php echo esc_html( $read_more ); ?><i class="fa fa-angle-right">&nbsp;</i></a>
											</div>
										</div>
							        </div>
							    </div>
							<?php endwhile; ?> 
				        </div>
				    </div>
				</div>
	        <?php } ?>	 		
	    </div>
	</div>
</div>
<!-- Blog-end -->
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">	
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php   
if ( function_exists( 'wp_body_open' ) )
wp_body_open();
?>
	<div id="wrapper">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'scoreline' ); ?></a>
		<div class="scoreline-header">
			<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 header">
					<?php 
					if (has_custom_logo()) { ?>
					    <?php the_custom_logo(); ?>
				    <?php }
				    if (display_header_text() == true) { ?> 
				    	<a href="<?php echo esc_url(home_url( '/' )); ?>">
				    		<h1 class="site-title"><?php echo esc_html(get_bloginfo('name')); ?></h1>
				    	</a>
				    <?php } ?>
				</div>
				
				<div class="col-md-6 col-sm-12 header-social">
					<?php   
                    $email_id = get_theme_mod('email_id');
                    $phone_no = get_theme_mod('phone_no');
                    if (!empty ($email_id) || !empty ($phone_no)) { ?>
						<span class="scoreline_email_id">
						  	<?php if (!empty ($email_id)) { ?>
						  		<a href="mailto:<?php echo esc_url( $email_id ); ?>"><i class="fa fa-envelope icon"></i> <?php echo esc_html( $email_id ); ?></a>
							<?php } ?> 
						</span>
					    <span class="scoreline_phone_no">
					    	<?php if (!empty ( $phone_no ) ) { ?>
								<a href="tel:<?php echo esc_url( $phone_no ); ?>"><i class="fa fa-phone icon"></i><?php echo esc_html( $phone_no ); ?></a>
							<?php } ?>
						</span>
					<?php } ?>	

					<?php 
					$header_social_media_in_enabled = absint(get_theme_mod('header_social_media_in_enabled', 1));
					if ($header_social_media_in_enabled == 1) {  ?>
						<div class="scoreline_social_media_head">
							<ul class="social-icons hd-sc">
								<?php 
								$fb_link = get_theme_mod('fb_link');
		                    	if (!empty ($fb_link)) { ?>
									<li class="facebook"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
								<?php }
								$twitter_link = get_theme_mod('twitter_link');
		                    	if (!empty ($twitter_link)) { ?>
									<li class="twitter"><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
								<?php } 
								$youtube_link = get_theme_mod('youtube_link');
		                    	if (!empty ($youtube_link)) { ?>
									<li class="youtube"><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
								<?php } 
								$linkedin_link = get_theme_mod('linkedin_link');
		                        if (!empty ($linkedin_link)) { ?>
									<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin-in"></i></a></li>
								<?php } 
								$instagram = get_theme_mod('instagram');
		                    	if (!empty ($instagram)) { ?>
		                    		<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
		                    	<?php } ?>
							</ul>
						</div>
					<?php } ?>
				</div>	
			</div>
			</div>
		</div>
		<!-- menu-start -->	
		<nav id="site-navigation" class="navbar main-navigation navbar-static-top home-menu-list" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="sr-only"><?php esc_html_e('Toggle navigation','scoreline'); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="navbar-collapse collapse">
					<?php wp_nav_menu( array(
						'theme_location' => 'primary-menu',
						'menu_class'     => 'nav navbar-nav navbar-right',
						'fallback_cb'    => 'weblizar_fallback_page_menu',
						'walker' 		 => new scoreline_nav_walker(),
						)
					);	
					?>			 
				</div>
			</div>
		</nav>
		<!-- menu-end -->
		<div id="content" class="site-content">
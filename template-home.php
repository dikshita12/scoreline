<?php //Template Name:HOME

get_header();

if ( scoreline_theme_is_companion_active() ) {

	get_template_part('sections/home','slider');

	if($sections = json_decode(get_theme_mod('home_reorder'),true)) {
		foreach ($sections as $section) {
			$data = "home_".$section;
			$data1 = absint(get_theme_mod($data, 1));
			if ($data1 == "1") {
				get_template_part('sections/home', $section);
			}
		}
	} else {

		get_template_part('sections/home', 'service');

		$editor_home = absint(get_theme_mod('editor_home', 1));
        if ($editor_home == 1) {
            get_template_part('sections/home', 'extra');
        }

		$blog_home = absint(get_theme_mod('blog_home', 1));

		if ($blog_home == 1) {
			get_template_part('sections/home','blog');
		}
	}

	$fc_home = absint(get_theme_mod('fc_home', 1));
    if ( $fc_home == 1) {
        get_template_part('sections/footer', 'callout');
    }
} else { 
    get_template_part( 'no', 'content' );
}	
get_footer();
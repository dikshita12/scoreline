<?php
/* Get the plugin */
if ( ! function_exists( 'scoreline_theme_is_companion_active' ) ) {
    function scoreline_theme_is_companion_active() {
        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if ( is_plugin_active(  'weblizar-companion/weblizar-companion.php' ) ) {
            return true;
        } else {
            return false;
        }
    }
}

require( get_template_directory() . '/core/default_menu_walker.php' );
require( get_template_directory() . '/core/scoreline_nav_walker.php' );
require( get_template_directory() . '/comment-function.php' );
require get_template_directory() . '/core/custom-header.php';
require( get_template_directory() . '/core/class-tgm-plugin-activation.php' );

function scoreline_style_theme(){
	//style
	wp_enqueue_style('scoreline-bootstrap',get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('fontawesome-all',  get_template_directory_uri() . '/css/font-awesome-5.11.2/css/all.css');
    wp_enqueue_style('scoreline-swiper', get_template_directory_uri(). '/css/swiper.css');
    wp_enqueue_style('scoreline-hover',  get_template_directory_uri(). '/css/hover.css');
	wp_enqueue_style('scoreline-color',  get_template_directory_uri() . '/css/color/color.css');
    wp_enqueue_style('scoreline-media',  get_template_directory_uri(). '/css/media.css');
	wp_enqueue_style('scoreline-animate',get_template_directory_uri(). '/css/animate.css');
    wp_enqueue_style('scoreline-style',  get_template_directory_uri(). '/style.css');
	
	//JS
	wp_enqueue_script('scoreline-bootstrap', get_template_directory_uri(). '/js/bootstrap.js', array('jquery'), true, true );  
    wp_enqueue_script('scoreline-navigation',get_template_directory_uri() . '/js/navigation.js', array('jquery'), true, true ); 
    wp_enqueue_script('scoreline-swiper',    get_template_directory_uri(). '/js/swiper.js', array('jquery'), true, true ); 
	wp_enqueue_script('scoreline-script-js', get_template_directory_uri().'/js/script.js', array('jquery'), true, true ); 
	

	$sticky_header = absint(get_theme_mod( 'sticky_header', '1' ));
	if ( $sticky_header =='1' ) {
        $sticky_header = true;
    } else {
        $sticky_header = false;
    }
	    
	$ajax_data = array(
        'sticky_header' => $sticky_header,
    );

    wp_enqueue_script( 'scoreline-ajax-front', get_template_directory_uri() . '/js/scoreline-ajax-front.js', array( 'jquery' ), true, true );
    wp_localize_script( 'scoreline-ajax-front', 'ajax_admin', array(
            'ajax_url'    => admin_url( 'admin-ajax.php' ),
            'admin_nonce' => wp_create_nonce( 'admin_ajax_nonce' ),
            'ajax_data'   => $ajax_data,
    ) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'scoreline_style_theme' );

/*After Theme Setup*/
add_action( 'after_setup_theme', 'scoreline_setup' ); 	
function scoreline_setup()
    {	
		global $content_width;
		//content width
		if ( ! isset( $content_width ) ) $content_width = 630; //px
	
		/*
	     * Make theme available for translation.
	     * Translations can be filed in the /languages/ directory.
	     * If you're building a theme based on Theme Palace, use a find and replace
	     * to change 'scoreline' to the name of your theme in all the template files.
	    */
		load_theme_textdomain( 'scoreline'); 

		add_theme_support( 'title-tag' );

		$args = array('default-color' => '#fff',);
        add_theme_support('custom-background', $args);

		add_editor_style();

		add_image_size('scoreline-post-thumb',340,210,true);

		add_theme_support( 'post-thumbnails' );

        add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'custom-logo', array(
							'height'      => 100,
							'width'       => 300,
							'flex-height' => false,
							'flex-width'  => false,
						) );

        add_theme_support( 'woocommerce' );

        add_theme_support( 'customize-selective-refresh-widgets' );
		
		// For Menu
		register_nav_menu( 'primary-menu', __( 'Primary Menu', 'scoreline' ) );
	}
//For home blog content
function scoreline_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'scoreline_excerpt_more');

// Register Sidebar
add_action( 'widgets_init', 'scoreline_widget_sidebar');
function scoreline_widget_sidebar() {
	/*sidebar*/
	register_sidebar( array(
		'name'         => __( 'Sidebar Primary', 'scoreline' ),
		'id'           => 'sidebar-primary',
		'description'  => __( 'The primary sidebar area', 'scoreline' ),
		'before_widget'=> '<div class="col-md-12 scoreline-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title'  => '</h2>'
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'scoreline' ),
		'id'            => 'footer-widget-area',
		'description'   => __( 'footer widget area', 'scoreline' ),
		'before_widget' => '<div class="col-md-3 col-sm-6 footer-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="col-md-12 footer-widget-heading"><h2>',
		'after_title'   => '</h2></div>'
	) );           
}

function scoreline_link_pages(){
	$defaults = array(
		'before'           => '<div class="scoreline_blog_pagination"><div class="scoreline_blog_pagi">' . __( 'Pages:','scoreline'  ),
		'after'            => '</div></div>',
		'link_before'      => '',
		'link_after'       => '',
		'next_or_number'   => 'number',
		'separator'        => ' ',
		'nextpagelink'     => __( 'Next page','scoreline' ),
		'previouspagelink' => __( 'Previous page','scoreline'),
		'pagelink'         => '%',
		'echo'             => 1
	);
	wp_link_pages( $defaults );
}

//breadcrums
function scoreline_breadcrumbs() {
    $delimiter = '';
    $home      = __('Home', 'scoreline' ); // text for the 'Home' link
    
    echo '<ul class="scoreline-breadcrumb">';
    global $post;
    $homeLink = esc_url(home_url());
    echo '<li><a href="' . esc_url($homeLink) . '">' . esc_html($home) . '</a></li>' . esc_html($delimiter) . ' ';
    if (is_category()) {
        global $wp_query;
        $cat_obj   = $wp_query->get_queried_object();
        $thisCat   = $cat_obj->term_id;
        $thisCat   = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0)
            echo wp_kses_post(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
        echo '<li>' . ' __("Archive by category","scoreline") "' . single_cat_title('', false) . '"' . '</li>';
    } elseif (is_day()) {
        echo '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a></li> ' . esc_html($delimiter) . ' ';
        echo '<li><a href="' . esc_url(get_month_link(get_the_time('Y'), get_the_time('m'))) . '">' . esc_html(get_the_time('F')) . '</a></li> ' . esc_html($delimiter) . ' ';
        echo '<li>' . esc_html(get_the_time('d')) . '</li>';
    } elseif (is_month()) {
        echo '<li><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a></li> ' . esc_html($delimiter) . ' ';
        echo '<li>' . esc_html(get_the_time('F')) . '</li>';
    } elseif (is_year()) {
        echo '<li>' . esc_html(get_the_time('Y') ). '</li>';
    } elseif (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
            $post_type = get_post_type_object(get_post_type());
            $slug = $post_type->rewrite;
            echo '<li><a href="' . esc_url($homeLink) . '/' . esc_url($slug['slug']) . '/">' . esc_html($post_type->labels->singular_name) . '</a></li> ' . esc_html($delimiter) . ' ';
            echo '<li>' . esc_html(get_the_title()) . '</li>';
        } else {
            $cat = get_the_category();
            $cat = $cat[0];
            echo '<li>' . wp_kses_post(get_the_title()) . '</li>';
        }
		
    } elseif (!is_single() && !is_page() && get_post_type() != 'post') {
        $post_type = get_post_type_object(get_post_type());
        echo '<li>' . esc_html($post_type->labels->singular_name) . '</li>';
    } elseif (is_attachment()) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID);
        $cat = $cat[0];
        echo wp_kses_post(get_category_parents($cat, TRUE, ' ' . $delimiter . ' '));
        echo '<li><a href="' . esc_url(get_permalink($parent)) . '">' . esc_html($parent->post_title) . '</a></li> ' . esc_html($delimiter) . ' ';
        echo '<li>' . esc_html(get_the_title()) . '</li>';
    } elseif (is_page() && !$post->post_parent) {
        echo '<li>' . esc_html(get_the_title()) . '</li>';
    } elseif (is_page() && $post->post_parent) {
        $parent_id = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<li><a href="' . esc_url(get_permalink($page->ID)) . '">' . esc_html(get_the_title($page->ID)) . '</a></li>';
            $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb)
            echo wp_kses_post($crumb) . ' ' . esc_html($delimiter) . ' ';
        echo '<li>' . esc_html(get_the_title()) . '</li>';
    } elseif (is_search()) {
        echo '<li>' . esc_html__("Search results for","scoreline")  . get_search_query() . '"' . '</li>';

    } elseif (is_tag()) {        
		echo '<li>' . esc_html__('Tag','scoreline') . single_tag_title('', false) . '</li>';
    } elseif (is_author()) {
        global $author;
        $userdata = get_userdata($author);
        echo '<li>' . esc_html__("Articles posted by","scoreline") . esc_html($userdata->display_name) . '</li>';
    } elseif (is_404()) {
        echo '<li>' . esc_html__("Error 404","scoreline") . '</li>';
    }
    
    echo '</ul>';
}

function scoreline_navigation() { ?>
	<div class="scoreline_blog_pagination">
		<div class="scoreline_blog_pagi">
			<span class="previous"><?php previous_posts_link(__('&laquo; Older Post', 'scoreline')); ?></span>
			<span class="next"><?php next_posts_link(__('Newer Post &raquo;','scoreline')); ?></span>
		</div>
	</div>
<?php }

/****--- Navigation for Single ---***/
function scoreline_navigation_posts() { ?>
	<div class="navigation_en">
		<nav id="scoreline_nav"> 
			<div class="col-md-6 nav-previous">
				<?php previous_post_link('&laquo; %link'); ?>
			</div>
			<div class="col-md-6 nav-next">
				<p><?php next_post_link('%link &raquo;'); ?></p>
			</div> 	
		</nav>
	</div>	
<?php } 

// For Plugin Activation
add_action('tgmpa_register','scoreline_plugin_recommend');
function scoreline_plugin_recommend(){
	$plugins = array(
        array(
            'name' => esc_html__('Weblizar Companion','scoreline'),
            'slug' => 'weblizar-companion',
            'required' => false,
        ),
    );
	tgmpa( $plugins );
}
?>
=== scoreline ===
Contributors: weblizar
Tags: blog, custom-logo, custom-menu, featured-images, footer-widgets, full-width-template, left-sidebar, one-column, right-sidebar, theme-options, threaded-comments, two-columns, custom-background, custom-header, grid-layout,  sticky-post, three-columns, translation-ready
Requires at least: 4.0
Tested up to: 5.5
Requires PHP: 7.0
Stable tag: 1.7.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Scoreline is a free WordPress theme best for travel and tourism business sites.

== Description ==
Scoreline is a multipurpose WordPress theme that is made specifically for sites dealing in travel and tourism business. This multipurpose Tour WordPress Theme best suits versatile businesses such as tourist agencies, tour operators, travel guides, photographic agencies, travel diaries, hotels, lifestyle, technology, traveling or journey blog, and lots more. It’s a very user-friendly and interactive Hotel WordPress Theme that supports the latest WordPress versions. It has an eye-catching design which attracts a number of hoteliers. Ample of personalization options are provided that eases the process of customizing the theme according to your choices. The Call to action button (CTA) is an element well-incorporated in the theme to redirect the users to take an action.

<a href="https://wordpress.org/themes/scoreline/">Discover more about the theme</a>.

== Frequently Asked Questions ==

= Where can i raise the theme issue? =

Please drop your issues here <a href="https://wordpress.org/support/theme/scoreline"> we'll try to triage issues reported on the theme forum, you'll get a faster response.

= Where can I read more about Scoreline? =

<a href="https://wordpress.org/themes/scoreline/">Theme detail page</a>

== Changelog ==

= 1.7.2 =
* Remove Admin Notice
* Rewmove Snowstorm
* Update Font-Awesome
* Update according to new guidline

= 1.7.1 =
* aBug Fix

= 1.7.0 =
* added new post leftsidebar template
* Fontawesome icons updated.

= 1.6.0 =
* added support to show/hide slider on homepage

= 1.5.6 =
* Readme.text file changed as per new rule.
* Minor changes in theme info notice.
* Minor issues fixed for customizer..

= 1.5.5 =
* Welcome page added.
* Minor issues fixed in header.

= 1.5.4 =
* Rating option added on admin panel.

= 1.5.3 =
* Sticky header option added.
* Go Enigma Premium option added on admin panel.

= 1.5.2 =
* Description update.

= 1.5.1 =
* One more slider added for home page.
* Plug-in Recommendation.

= 1.5 =
* Snow effect option added.
* Rating banner dismiss option added.

= 1.4 =
* Quick edit.
* Editor.
* Icon picker.
* Front Page Section Manager.
* Rating banner.

= 1.3 =
* Homepage setup page added.

= 1.2 =
* Minor changes in custom header.

= 1.1 =
* FontAwesome Updated.
* snow effect remved for customizer.
* Custome header added(Randomizesd).

= 1.0 =
* FontAwesome Updated.
* snow effect aded for customizer.
* Custome header added.

= 0.9 =
* Other themes and plugin page added only showing.

= 0.8 =
* Issue Fix.

= 0.7
* Fixing some minor issues.

= 0.6
* Minor Changes.

= 0.5
* Released.

== Resources ==

= BUNDELED  =

* Bootstrap v3.3.5 (http://getbootstrap.com)
  Copyright 2011-2015 Twitter, Inc.
  Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

* Animations CSS 
  animate.css -http://daneden.me/animate
  Version - 3.5.0
  Licensed under the MIT license - http://opensource.org/licenses/MIT
  Copyright (c) 2015 Daniel Eden

* Font Awesome Free 5.8.1 by @fontawesome - https://fontawesome.com
  License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

* Hover.css (http://ianlunn.github.io/Hover/)
  Version: 2.0.2
  Made available under a MIT License:
  http://www.opensource.org/licenses/mit-license.php

*  Swiper 3.3.1
   Copyright 2016, Vladimir Kharlampidi
   The iDangero.us
   http://www.idangero.us/
   Licensed under MIT

   == Image Resources ==
   
  = Slider Image =
   https://unsplash.com/photos/hBuwVLcYTnA

  = Service Image
  https://images.unsplash.com/photo-1488901517307-a8f6582fa081?ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80
  https://images.unsplash.com/photo-1511649475669-e288648b2339?ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80
  https://images.unsplash.com/photo-1489171078254-c3365d6e359f?ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80

  All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)

== TGMPA ==
	Source : https://github.com/TGMPA/tgm-example-plugin/blob/master/LICENSE
<div class="col-md-12">
	<div class="error_404">
		<h2><?php esc_url(get_template_directory_uri());('404','scoreline'); ?></h2>
		<h4><?php esc_url(get_template_directory_uri());('Whoops... Post Not Found !!!','scoreline'); ?></h4>
		<p><?php esc_url(get_template_directory_uri());('We are sorry, but the page you are looking for does not exist.','scoreline'); ?></p>
		<p><a href="<?php echo esc_url(home_url( '/' )); ?>"><button id="commentSubmit" type="submit"><?php esc_url(get_template_directory_uri());('Go To Homepage','scoreline'); ?></button></a></p>
	</div>
</div>
var window, document, jQuery;
(function (jQuery) {
    "use strict";
    if ( ajax_admin.ajax_data.sticky_header == true ) {

        jQuery(document).ready(function() {
            jQuery(window).scroll(function () {
            if( jQuery(window).width() > 768) {
                if (jQuery(this).scrollTop() > 220) {
                jQuery('.home-menu-list').addClass('sticky-head');
                }
                else {
            jQuery('.home-menu-list').removeClass('sticky-head');
            }
            }
                else {
                if (jQuery(this).scrollTop() > 250) {
                    jQuery('.home-menu-list').addClass('sticky-head');
                }else {
            jQuery('.home-menu-list').removeClass('sticky-head');
            }
                }               
            });
            
        });  
    }

})(jQuery);